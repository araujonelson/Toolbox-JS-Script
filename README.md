Toolbox assessment #1.

Script en Javascript que recibe un array de enteros y devuelve un array de String bajo las siguientes reglas:

• Devuelve Fizz si el número es divisible por 3 o si incluye un 3 en el número.
• Devuelve Buzz si el número es divisible por 5 o si incluye un 5 en el número.
• Devuelve FizzBuzz si el número es divisible por 3 y por 5.