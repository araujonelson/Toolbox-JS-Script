let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

function toolbox(array)
{
  let newArray = [];

  for (let current of array){
    if((current % 3 === 0)&&(current % 5 === 0)) {
      newArray.push("FizzBuzz");
    }
    else if((current % 5 === 0)||(current.toString().match(/5/))){
      newArray.push("Buzz");
    }
    else if((current % 3 === 0)||(current.toString().match(/3/))){
      newArray.push("Fizz");
    }
  }

  return newArray;
}

//Testing using QUnit.
QUnit.test( "Unique test", function( assert ) {
  assert.ok((toolbox([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]).toString()) == 'Fizz,Buzz,Fizz,Fizz,Buzz,Fizz,Fizz,FizzBuzz', "Passed!" );
  assert.ok((toolbox([3, 3, 3, 3, 3, 3, 3, 3, 3, 3]).toString()) == 'Fizz,Fizz,Fizz,Fizz,Fizz,Fizz,Fizz,Fizz,Fizz,Fizz', "Passed!" );
  assert.ok((toolbox([3, 5, 15]).toString()) == 'Fizz,Buzz,FizzBuzz', "Passed!" );
});
